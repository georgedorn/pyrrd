import tempfile
from unittest import TestCase

from pyrrd.backend.external import create

# Dumb stdout capturer, for testing functions that print
from contextlib import contextmanager
from pyrrd.compat import StringIO

@contextmanager
def captured_output():
    import sys
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err


class RRDBaseTestCase(TestCase):

    def setUp(self):
        self.rrdfile = tempfile.NamedTemporaryFile()
        self.filename = self.rrdfile.name
        parameters = (
            "--start 920804400 "
            "DS:speed:COUNTER:600:-10.1:10.3 "
            "RRA:AVERAGE:0.5:1:24 "
            "RRA:AVERAGE:0.5:6:10")
        create(self.filename, parameters)
