"""
Compatability module to resolve python2 / python3 concerns.
"""
import six

# StringIO 
try:
    from cStringIO import StringIO  # noqa
except ImportError:
    from io import StringIO  # noqa

# unicode()
def unicode(s):
    if six.PY3:
        return str(s)
    else:
        return six.u(s)
