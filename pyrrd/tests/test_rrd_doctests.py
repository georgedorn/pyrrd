import os
import tempfile
from unittest.case import TestCase

from pyrrd.rrd import validateDSName, validateDSType, validateRRACF, DataSource, RRA, RRD
from pyrrd.testing.base import captured_output


class TestValidators(TestCase):
    def test_validateDSName(self):
        with self.assertRaises(ValueError) as exc:
            validateDSName('Zaphod Beeble-Brox!')
        self.assertEqual(str(exc.exception), 'Names must consist only of the characters A-Z, a-z, 0-9, _')

        for vname in ('Zaphod_Bee_Brox', 'a'*18):
            validateDSName(vname)

        with self.assertRaises(ValueError) as exc:
            validateDSName('a'*19)
        self.assertEqual(str(exc.exception), 'Names must be shorter than 19 characters')

    def test_validateDSType(self):
        self.assertEqual(validateDSType('counter'), 'COUNTER')
        with self.assertRaises(ValueError) as exc:
            validateDSType('ford prefect')
        self.assertEqual(str(exc.exception),
                         'A data source type must be one of the following: GAUGE COUNTER DERIVE ABSOLUTE COMPUTE')

    def test_validateRRACF(self):
        self.assertEqual(validateRRACF('Max'), 'MAX')
        for bad_fn in ('Maximum', 'Trisha MacMillan'):
            with self.assertRaises(ValueError) as exc:
                validateRRACF(bad_fn)
            self.assertEqual(str(exc.exception),
                             "An RRA's consolidation function must be one of the following: AVERAGE MIN MAX LAST HWPREDICT SEASONAL DEVSEASONAL DEVPREDICT FAILURES")


class TestRRD(TestCase):
    def test_init(self):
        dss = []
        rras = []
        rrdfile = tempfile.NamedTemporaryFile()
        dss.append(DataSource(dsName='speed', dsType='COUNTER', heartbeat=600))
        rras.append(RRA(cf='AVERAGE', xff=0.5, steps=1, rows=24))
        rras.append(RRA(cf='AVERAGE', xff=0.5, steps=6, rows=10))
        rrd = RRD(rrdfile.name, ds=dss, rra=rras, start=920804400)
        rrd.create()
        
        self.assertEqual(os.path.exists(rrdfile.name), True)

        rrd.bufferValue('920805600', '12363')
        rrd.bufferValue('920805900', '12363')
        rrd.bufferValue('920806200', '12373')
        rrd.bufferValue('920806500', '12383')
        rrd.update()
        rrd.bufferValue('920806800', '12393')
        rrd.bufferValue('920807100', '12399')
        rrd.bufferValue('920807400', '12405')
        rrd.bufferValue('920807700', '12411')
        rrd.bufferValue('920808000', '12415')
        rrd.bufferValue('920808300', '12420')
        rrd.bufferValue('920808600', '12422')
        rrd.bufferValue('920808900', '12423')
        rrd.update()
        self.assertEqual(len(rrd.values), 0)

    def test_bufferValue(self):
        my_rrd = RRD('somefile')
        my_rrd.bufferValue('1000000', 'value')

        with captured_output() as (out, _):
            my_rrd.update(debug=True, dryRun=True)
        self.assertEqual(out.getvalue().strip(), "('somefile', ['1000000:value'])")

        with captured_output() as (out, _):
            my_rrd.update(template='ds0', debug=True, dryRun=True),
        self.assertEqual(out.getvalue().strip(),
                         "('somefile', ['--template', 'ds0', '1000000:value'])")
        my_rrd.values = []

        my_rrd.bufferValue('1000000:value')
        with captured_output() as (out, _):
            my_rrd.update(debug=True, dryRun=True)
        self.assertEqual(out.getvalue().strip(),
                         "('somefile', ['1000000:value'])")

        with captured_output() as (out, _):
            my_rrd.update(template='ds0', debug=True, dryRun=True)
        self.assertEqual(out.getvalue().strip(),
                         "('somefile', ['--template', 'ds0', '1000000:value'])")
        my_rrd.values = []

        my_rrd.bufferValue('1000000', 'value1', 'value2')
        my_rrd.bufferValue('1000001', 'value3', 'value4')
        with captured_output() as (out, _):
            my_rrd.update(debug=True, dryRun=True)
        self.assertEqual(out.getvalue().strip(),
                         "('somefile', ['1000000:value1:value2', '1000001:value3:value4'])")

        with captured_output() as (out, _):
            my_rrd.update(template=u'ds1:ds0', debug=True, dryRun=True)
        self.assertEqual(out.getvalue().strip(),
                         "('somefile', ['--template', 'ds1:ds0', '1000000:value1:value2', '1000001:value3:value4'])")
        my_rrd.values = []

        my_rrd.bufferValue('1000000:value')
        my_rrd.bufferValue('1000001:anothervalue')

        with captured_output() as (out, _):
            my_rrd.update(debug=True, dryRun=True)
        self.assertEqual(out.getvalue().strip(),
                         "('somefile', ['1000000:value', '1000001:anothervalue'])")

        with captured_output() as (out, _):
            my_rrd.update(template='ds0', debug=True, dryRun=True)
        self.assertEqual(out.getvalue().strip(),
                         "('somefile', ['--template', 'ds0', '1000000:value', '1000001:anothervalue'])")

    def test_load(self):
        dss = []
        rras = []
        rrdfile = tempfile.NamedTemporaryFile()
        dss.append(DataSource(dsName='speed', dsType='COUNTER', heartbeat=600))
        rras.append(RRA(cf='AVERAGE', xff=0.5, steps=1, rows=24))
        rras.append(RRA(cf='AVERAGE', xff=0.5, steps=6, rows=10))
        rrd = RRD(rrdfile.name, ds=dss, rra=rras, start=920804400)
        rrd.create()
        self.assertEqual(os.path.exists(rrdfile.name), True)

        # Add some values:
        rrd.bufferValue('920805600', '12363')
        rrd.bufferValue('920805900', '12363')
        rrd.bufferValue('920806200', '12373')
        rrd.bufferValue('920806500', '12383')
        rrd.update()

        # Let's create another one, using the source file we just created. Note
        # that by passing the "read" mode, were letting the RRD class know that
        # it should call load() immediately, thus giving us read-access to the
        # file's data.
        rrd2 = RRD(rrdfile.name, mode="r")

        # Now let's load the data from self.filename:
        top_level_attrs = rrd2.getData()
        self.assertEqual(top_level_attrs["lastupdate"], 920806500)
        self.assertEqual(top_level_attrs["filename"], rrdfile.name)
        self.assertEqual(top_level_attrs["step"], 300)
        self.assertEqual(len(rrd2.ds), 1)
        self.assertEqual(len(rrd2.rra), 2)
        self.assertEqual(sorted(rrd2.ds[0].getData().keys()),
                         ['last_ds', 'max', 'min', 'minimal_heartbeat', 'name', 'rpn', 'type', 'unknown_sec', 'value'])
        self.assertEqual(sorted(rrd2.rra[1].getData().keys()),
                         ['alpha', 'beta', 'cdp_prep', 'cf', 'database', 'ds', 'gamma', 'pdp_per_row', 'rows', 'rra_num', 'seasonal_period', 'steps', 'threshold', 'window_length', 'xff'])

        # Finally, a comparison:
        self.assertEqual(rrd.lastupdate, rrd2.lastupdate)
        self.assertEqual(rrd.filename, rrd2.filename)
        self.assertEqual(rrd.step, rrd2.step)


class TestDataSource(TestCase):
    def test_init(self):
        ds = DataSource(dsName='speed', dsType='COUNTER', heartbeat=600)
        self.assertEqual(str(ds), "DS:speed:COUNTER:600:U:U")


class TestRRA(TestCase):
    def test_init(self):
        rra1 = RRA(cf='AVERAGE', xff=0.5, steps=1, rows=24)
        self.assertEqual(str(rra1), 'RRA:AVERAGE:0.5:1:24')
        
        rra2 = RRA(cf='AVERAGE', xff=0.5, steps=6, rows=10)
        self.assertEqual(str(rra2), 'RRA:AVERAGE:0.5:6:10')
