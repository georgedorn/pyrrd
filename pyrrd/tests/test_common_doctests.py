import unittest

from pyrrd.backend.common import coerce, iterParse, buildParameters

class Test(unittest.TestCase):
    def test_coerce(self):
        self.assertEqual(str(coerce("NaN")), 'nan')
        self.assertEqual(str(coerce("nan")), 'nan')
        self.assertEqual(coerce("1"), 1.0)
        self.assertTrue(0.039 < coerce("4.0000000000e-02") < 0.041)
        self.assertTrue(0.039 < coerce(4.0000000000e-02) < 0.041)
        self.assertIsNone(coerce("Unkn"))
        self.assertIsNone(coerce("u"))

    def test_iterParse(self):
        lines = [' 920804700: nan',
                 ' 920805000: 4.0000000000e-02',
                 ' 920805300: 2.0000000000e-02',
                 ' 920805600: 0.0000000000e+00',
                 ' 920805900: 0.0000000000e+00',
                 ' 920806200: 3.3333333333e-02',
                 ' 920806500: 3.3333333333e-02',
                 ' 920806800: 3.3333333333e-02',
                 ' 920807100: 2.0000000000e-02',
                 ' 920807400: 2.0000000000e-02',
                 ' 920807700: 2.0000000000e-02',
                 ' 920808000: 1.3333333333e-02',
                 ' 920808300: 1.6666666667e-02',
                 ' 920808600: 6.6666666667e-03',
                 ' 920808900: 3.3333333333e-03',
                 ' 920809200: nan']
        g = iterParse(lines)
        self.assertEqual(str(next(g)), "(920804700, nan)")
        self.assertEqual(next(g), (920805000, 0.04))
        self.assertEqual(len(list(g)), len(lines) - 2)

    def test_buildParameters(self):
        class TestClass(object):
            pass
        testClass = TestClass()
        testClass.a = 1
        testClass.b = "2"
        testClass.c = 3
        testClass.d = True

        self.assertEqual(buildParameters(testClass, ["a", "b"]), ['--a', u'1', '--b', u'2'])
        
        testClass.b = None
        self.assertEqual(buildParameters(testClass, ["a", "b"]), ['--a', u'1'])

        self.assertEqual(buildParameters(testClass, ["a", "d"]), ['--a', u'1', '--d'])
