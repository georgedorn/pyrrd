import tempfile
import os
import unittest

from pyrrd.backend.external import create, update, fetch, dump, load, graph


class TestExternal(unittest.TestCase):

    def test_create(self):
        rrdfile = tempfile.NamedTemporaryFile()
        parameters = ' --start 920804400'
        parameters += ' DS:speed:COUNTER:600:U:U'
        parameters += ' RRA:AVERAGE:0.5:1:24'
        parameters += ' RRA:AVERAGE:0.5:6:10'
        create(rrdfile.name, parameters)
        self.assertEqual(os.path.exists(rrdfile.name), True)

    def test_update(self):
        rrdfile = tempfile.NamedTemporaryFile()
        parameters = ' --start 920804400'
        parameters += ' DS:speed:COUNTER:600:U:U'
        parameters += ' RRA:AVERAGE:0.5:1:24'
        parameters += ' RRA:AVERAGE:0.5:6:10'
        create(rrdfile.name, parameters)
        self.assertEqual(os.path.exists(rrdfile.name), True)

        original = open(rrdfile.name, 'rb').read()

        update(rrdfile.name, '920804700:12345 920805000:12357 920805300:12363')
        update(rrdfile.name, '920805600:12363 920805900:12363 920806200:12373')
        update(rrdfile.name, '920806500:12383 920806800:12393 920807100:12399')
        update(rrdfile.name, '920807400:12405 920807700:12411 920808000:12415')
        update(rrdfile.name, '920808300:12420 920808600:12422 920808900:12423')

        updated = open(rrdfile.name, 'rb').read()

        # Updating the file should have changed it
        self.assertNotEqual(original, updated)

    def test_fetch(self):
        rrdfile = tempfile.NamedTemporaryFile()
        parameters = ' --start 920804400'
        parameters += ' DS:speed:COUNTER:600:U:U'
        parameters += ' RRA:AVERAGE:0.5:1:24'
        parameters += ' RRA:AVERAGE:0.5:6:10'
        create(rrdfile.name, parameters)

        update(rrdfile.name, '920804700:12345 920805000:12357 920805300:12363')
        update(rrdfile.name, '920805600:12363 920805900:12363 920806200:12373')
        update(rrdfile.name, '920806500:12383 920806800:12393 920807100:12399')
        update(rrdfile.name, '920807400:12405 920807700:12411 920808000:12415')
        update(rrdfile.name, '920808300:12420 920808600:12422 920808900:12423')

        results = fetch(rrdfile.name, 'AVERAGE --start 920804400 --end 920809200')
        self.assertEqual(sorted(results["ds"].keys()), ['speed'])

        self.assertEqual(results["ds"]["speed"][0], (920805000, 0.04))

        keys = sorted(results["time"].keys())
        self.assertEqual(len(keys), 16)
        self.assertEqual(keys[0:6], [920805000, 920805300, 920805600, 920805900, 920806200, 920806500])
        self.assertEqual(results["time"][920805000], {'speed': 0.04})

    def test_dump(self):
        rrdfile = tempfile.NamedTemporaryFile()
        parameters = ' --start 920804400'
        parameters += ' DS:speed:COUNTER:600:U:U'
        parameters += ' RRA:AVERAGE:0.5:1:24'
        parameters += ' RRA:AVERAGE:0.5:6:10'
        create(rrdfile.name, parameters)

        xml = dump(rrdfile.name)
        xmlBytes = len(xml)
        self.assertTrue(3300 < xmlBytes < 4000)
        xmlCommentCheck = '<!-- Round Robin Database Dump'
        self.assertTrue(xmlCommentCheck in xml[0:200])

        xmlfile = tempfile.NamedTemporaryFile()
        dump(rrdfile.name, xmlfile.name)

        self.assertTrue(os.path.exists(xmlfile.name))

    def test_load(self):
        rrdfile = tempfile.NamedTemporaryFile()
        parameters = ' --start 920804400'
        parameters += ' DS:speed:COUNTER:600:U:U'
        parameters += ' RRA:AVERAGE:0.5:1:24'
        parameters += ' RRA:AVERAGE:0.5:6:10'
        create(rrdfile.name, parameters)
        tree = load(rrdfile.name)
        self.assertEqual([x.tag for x in tree], ['version', 'step', 'lastupdate', 'ds', 'rra', 'rra'])
    
    def test_graph(self):
        rrdfile = tempfile.NamedTemporaryFile()
        graphfile = tempfile.NamedTemporaryFile()
        parameters = ' --start 920804400'
        parameters += ' DS:speed:COUNTER:600:U:U'
        parameters += ' RRA:AVERAGE:0.5:1:24'
        parameters += ' RRA:AVERAGE:0.5:6:10'
        create(rrdfile.name, parameters)
        update(rrdfile.name, '920804700:12345 920805000:12357 920805300:12363')
        update(rrdfile.name, '920805600:12363 920805900:12363 920806200:12373')
        update(rrdfile.name, '920806500:12383 920806800:12393 920807100:12399')
        update(rrdfile.name, '920807400:12405 920807700:12411 920808000:12415')
        update(rrdfile.name, '920808300:12420 920808600:12422 920808900:12423')

        parameters = ' --start 920804400 --end 920808000'
        parameters += ' --vertical-label km/h'
        parameters += ' DEF:myspeed=%s:speed:AVERAGE' % rrdfile.name
        parameters += ' CDEF:realspeed=myspeed,1000,*'
        parameters += ' CDEF:kmh=myspeed,3600,*'
        parameters += ' CDEF:fast=kmh,100,GT,kmh,0,IF'
        parameters += ' CDEF:good=kmh,100,GT,0,kmh,IF'
        parameters += ' HRULE:100#0000FF:"Maximum allowed"'
        parameters += ' AREA:good#00FF00:"Good speed"'
        parameters += ' AREA:fast#00FFFF:"Too fast"'
        parameters += ' LINE2:realspeed#FF0000:Unadjusted'
        graph(graphfile.name, parameters)
        self.assertTrue(os.path.exists(graphfile.name))


