from unittest.case import TestCase
import tempfile
import os


from pyrrd.graph import (
    validateVName,
    escapeColons,
    validateObjectType,
    validateImageFormat,
    DataDefinition,
    DEF,
    VariableDefinition,
    VDEF,
    CDEF,
    Print,
    GraphPrint,
    GPRINT,
    GraphComment,
    Line,
    LINE,
    Area,
    AREA,
    GraphTick,
    ColorAttributes,
    Graph)

from pyrrd.rrd import RRD, RRA, DS

class TestValidators(TestCase):

    def test_validateVName(self):
        with self.assertRaises(ValueError) as exc:
            validateVName('Zaphod Beeble-Brox!')
        self.assertEqual(str(exc.exception), 'Names must consist only of the characters A-Z, a-z, 0-9, -, _')

        for name in ('Zaphod_Beeble-Brox', 'a'*32, 'a'*254, 'a'*255):
            self.assertEqual(name, validateVName(name))

        with self.assertRaises(ValueError) as exc:
            validateVName('a'*256)
        self.assertEqual(str(exc.exception), "Names must be shorter than 255 characters")

    def test_escapeColons(self):
        self.assertEqual(escapeColons('now'), 'now')
        self.assertEqual(escapeColons('end-8days8hours'), 'end-8days8hours')
        self.assertEqual(escapeColons('13:00'), '13\:00')

    def test_validateObjectType(self):
        my_list = [1,2,3,4,5]
        self.assertEqual(validateObjectType(my_list, list), [1, 2, 3, 4, 5])
        with self.assertRaises(TypeError) as exc:
            validateObjectType(my_list, dict)
        self.assertEqual(str(exc.exception), 'list instance is not of type dict')

    def test_validateImageFormat(self):
        with self.assertRaises(ValueError) as exc:
            validateImageFormat('txt')
        self.assertEqual(str(exc.exception), 'The image format must be one of the following: PNG SVG EPS PDF')

        with self.assertRaises(ValueError) as exc:
            validateImageFormat('jpg')
        self.assertEqual(str(exc.exception), 'The image format must be one of the following: PNG SVG EPS PDF')

        self.assertEqual(validateImageFormat('png'), 'PNG')


class TestDataDefiner(TestCase):
    
    def test_init(self):
        def1 = DataDefinition(vname='ds0a', rrdfile='/home/rrdtool/data/router1.rrd', dsName='ds0', cdef='AVERAGE')
        self.assertEqual(str(def1), 'DEF:ds0a=/home/rrdtool/data/router1.rrd:ds0:AVERAGE')
        self.assertEqual(def1.__repr__(),
                        'DEF:ds0a=/home/rrdtool/data/router1.rrd:ds0:AVERAGE')

    def test_post_init(self):
        def2 = DataDefinition(rrdfile='/home/rrdtool/data/router1.rrd')
        def2.vname = 'ds0b'
        def2.dsName = 'ds0'
        def2.cdef = 'AVERAGE'
        def2.step = 1800
        self.assertEqual(str(def2), "DEF:ds0b=/home/rrdtool/data/router1.rrd:ds0:AVERAGE:step=1800")

    def test_post_init_file(self):
        def3 = DEF(vname='ds0c', dsName='ds0', step=7200)
        def3.rrdfile = '/home/rrdtool/data/router1.rrd'
        self.assertEqual(str(def3), 'DEF:ds0c=/home/rrdtool/data/router1.rrd:ds0:AVERAGE:step=7200')

    def test_bad_init(self):
        def4 = DEF()
        with self.assertRaises(ValueError) as exc:
            str(def4)
        self.assertEqual(str(exc.exception),
                         'vname, rrdfile, dsName, and cdef are all required attributes and cannot be None.')
        def4.rrdfile = '/home/rrdtool/data/router2.rrd'
        with self.assertRaises(ValueError) as exc:
            str(def4)
        self.assertEqual(str(exc.exception),
                         'vname, rrdfile, dsName, and cdef are all required attributes and cannot be None.')
        def4.vname = 'ds1a'
        with self.assertRaises(ValueError) as exc:
            str(def4)
        self.assertEqual(str(exc.exception),
                         'vname, rrdfile, dsName, and cdef are all required attributes and cannot be None.')
        def4.dsName = 'ds1'
        self.assertEqual(str(def4), 'DEF:ds1a=/home/rrdtool/data/router2.rrd:ds1:AVERAGE')


class TestVariableDefinition(TestCase):

    def test_init(self):
        def1 = DEF(rrdfile='/home/rrdtool/data/router1.rrd', vname='ds0a', dsName='ds0')
        def2 = DEF(rrdfile='/home/rrdtool/data/router1.rrd', vname='ds1a', dsName='ds1')
        rpnmax = '%s,MAXIMUM'
        rpnmin = '%s,MINIMUM'
        rpnavg = '%s,AVERAGE'
        rpnpct = '%s,%s,PERCENT'
        vdef1 = VariableDefinition(vname='ds0max', rpn=rpnmax % def1.dsName)
        self.assertEqual(str(vdef1), 'VDEF:ds0max=ds0,MAXIMUM')
        vdef2 = VDEF(vname='ds0avg', rpn=rpnavg % def1.dsName)
        self.assertEqual(str(vdef2), 'VDEF:ds0avg=ds0,AVERAGE')
        vdef3 = VDEF(vname='ds0min', rpn=rpnmin % def1.dsName)
        self.assertEqual(str(vdef3), 'VDEF:ds0min=ds0,MINIMUM')
        vdef4 = VDEF(vname='ds1pct', rpn=rpnpct % (def2.dsName, 95))
        self.assertEqual(str(vdef4), 'VDEF:ds1pct=ds1,95,PERCENT')


class TestCalculationDefinition(TestCase):
    
    def test_init(self):
        someDSN = 'mydata'
        cdef1 = CDEF(vname='mydatabits', rpn='%s,8,*' % someDSN)
        self.assertEqual(str(cdef1), 'CDEF:mydatabits=mydata,8,*')


class TestPrint(TestCase):
    def test_init(self):
        def1 = DEF(rrdfile='/home/rrdtool/data/router1.rrd', vname='ds0a', dsName='ds0')
        vdef1 = VariableDefinition(vname='ds0max', rpn='%s,MAXIMUM' % def1.dsName)
        prnFmt = "%6.2lf %Sbps"
        prn = Print(vdef1, prnFmt)
        self.assertEqual(str(prn), 'PRINT:ds0max:"%6.2lf %Sbps"')

class TestGraphPrint(TestCase):
    def test_init(self):
        def1 = DEF(rrdfile='/home/rrdtool/data/router1.rrd', vname='ds0a', dsName='ds0')
        vdef1 = VariableDefinition(vname='ds0max', rpn='%s,MAXIMUM' % def1.dsName)
        prnFmt = '%6.2lf %Sbps'
        prn = GraphPrint(vdef1, prnFmt)
        self.assertEqual(str(prn), 'GPRINT:ds0max:"%6.2lf %Sbps"')

class TestGraphComment(TestCase):
    def test_init(self):
        cmt = GraphComment('95th percentile')
        self.assertEqual(len(str(cmt)), 26)
        cmt = GraphComment('95th percentile', autoNewline=False)
        self.assertEqual(len(str(cmt)), 25)
        self.assertEqual(str(cmt), 'COMMENT:"95th percentile"')


class TestLine(TestCase):
    def test_init(self):
        def1 = DEF(rrdfile='/home/rrdtool/data/router1.rrd', vname='ds0a', dsName='ds0')
        vdef1 = VariableDefinition(vname='ds0max', rpn='%s,MAXIMUM' % def1.dsName)

        # Now let's do some lines...
        line = Line(1, value='ds0max', color='#00ff00', legend="Max")
        self.assertEqual(str(line), 'LINE1:ds0max#00ff00:"Max"')
        self.assertEqual(str(LINE(2, defObj=def1, color='#0000ff')),
                         'LINE2:ds0a#0000ff')
        self.assertEqual(str(LINE(1, defObj=vdef1, color='#ff0000')),
                         'LINE1:ds0max#ff0000')
        with self.assertRaises(Exception) as exc:
            LINE(1, color='#ff0000')
        self.assertEqual(str(exc.exception), 'You must provide either a value or a definition object.')

        with self.assertRaises(ValueError) as exc:
            LINE(1, value=vdef1, color='#ff0000')
        self.assertEqual(str(exc.exception),
                         "The parameter 'value' must be either a string or an integer.")

class TestArea(TestCase):
    def test_init(self):
        def1 = DEF(rrdfile='/home/rrdtool/data/router1.rrd', vname='ds0a', dsName='ds0')
        vdef1 = VariableDefinition(vname='ds0max', rpn='%s,MAXIMUM' % def1.dsName)

        # Now let's do some areas...
        self.assertEqual(str(Area(value='ds0a', color='#cccccc', legend='Raw Router Data')),
                         'AREA:ds0a#cccccc:"Raw Router Data"')
        self.assertEqual(str(AREA(defObj=vdef1, color='#cccccc', legend='Max Router Data', stack=True)),
                         'AREA:ds0max#cccccc:"Max Router Data":STACK')


class TestGraphTick(TestCase):
    def test_init(self):
        def1 = DEF(rrdfile='/home/rrdtool/data/router1.rrd', vname='ds0a', dsName='ds0')

        self.assertEqual(str(GraphTick(def1,'#ffffff',0.3,'Alarm!')),
                         'TICK:ds0a#ffffff:0.3:"Alarm!"')


class TestColorAttributes(TestCase):
    def test_init(self):
        self.assertEqual(str(ColorAttributes(background='#000000', axis='#FFFFFF')),
                         'AXIS#FFFFFF --color BACK#000000')
        ca = ColorAttributes()
        ca.back = '#333333'
        ca.canvas = '#333333'
        ca.shadea = '#000000'
        ca.shadeb = '#111111'
        ca.mgrid = '#CCCCCC'
        ca.axis = '#FFFFFF'
        ca.frame = '#AAAAAA'
        ca.font = '#FFFFFF'
        ca.arrow = '#FFFFFF'
        self.assertEqual(str(ca), 'ARROW#FFFFFF --color AXIS#FFFFFF --color BACK#333333 --color CANVAS#333333 --color FONT#FFFFFF --color FRAME#AAAAAA --color MGRID#CCCCCC --color SHADEA#000000 --color SHADEB#111111')


class TestGraph(TestCase):
    def test_init(self):
        dss = []
        rras = []
        tfile = tempfile.NamedTemporaryFile()
        filename = tfile.name
        ds1 = DS(dsName='speed', dsType='COUNTER', heartbeat=600)
        dss.append(ds1)
        rra1 = RRA(cf='AVERAGE', xff=0.5, steps=1, rows=24)
        rra2 = RRA(cf='AVERAGE', xff=0.5, steps=6, rows=10)
        rras.extend([rra1, rra2])
        my_rrd = RRD(filename, ds=dss, rra=rras, start=920804400)
        my_rrd.create()
        self.assertEqual(os.path.exists(filename), True)

        my_rrd.bufferValue('920805600', '12363')
        my_rrd.bufferValue('920805900', '12363')
        my_rrd.bufferValue('920806200', '12373')
        my_rrd.bufferValue('920806500', '12383')
        my_rrd.update()
        my_rrd.bufferValue('920806800', '12393')
        my_rrd.bufferValue('920807100', '12399')
        my_rrd.bufferValue('920807400', '12405')
        my_rrd.bufferValue('920807700', '12411')
        my_rrd.bufferValue('920808000', '12415')
        my_rrd.bufferValue('920808300', '12420')
        my_rrd.bufferValue('920808600', '12422')
        my_rrd.bufferValue('920808900', '12423')
        my_rrd.update()
    
        # Let's set up the objects that will be added to the graph
        def1 = DEF(rrdfile=my_rrd.filename, vname='myspeed', dsName=ds1.name)
        cdef1 = CDEF(vname='kmh', rpn='%s,3600,*' % def1.vname)
        cdef2 = CDEF(vname='fast', rpn='kmh,100,GT,kmh,0,IF')
        cdef3 = CDEF(vname='good', rpn='kmh,100,GT,0,kmh,IF')
        vdef1 = VDEF(vname='mymax', rpn='%s,MAXIMUM' % def1.vname)
        vdef2 = VDEF(vname='myavg', rpn='%s,AVERAGE' % def1.vname)
        line1 = LINE(value=100, color='#990000', legend='Maximum Allowed')
        area1 = AREA(defObj=cdef3, color='#006600', legend='Good Speed')
        area2 = AREA(defObj=cdef2, color='#CC6633', legend='Too Fast')
        line2 = LINE(defObj=vdef2, color='#000099', legend='My Average', stack=True)
        gprint1 = GPRINT(vdef2, '%6.2lf kph')
    
        # Let's configure some custom colors for the graph
        ca = ColorAttributes()
        ca.back = '#333333'
        ca.canvas = '#333333'
        ca.shadea = '#000000'
        ca.shadeb = '#111111'
        ca.mgrid = '#CCCCCC'
        ca.axis = '#FFFFFF'
        ca.frame = '#AAAAAA'
        ca.font = '#FFFFFF'
        ca.arrow = '#FFFFFF'
    
        # Now that we've got everything set up, let's make a graph
        graphfile = tempfile.NamedTemporaryFile()
        g = Graph(graphfile.name, start=920805000, end=920810000, vertical_label='km/h', color=ca, imgformat='png')
        g.data.extend([def1, cdef1, cdef2, cdef3, vdef1, vdef2, line1, area1, area2, line2, gprint1])
        g.write()
        self.assertEqual(os.path.exists(graphfile.name), True)
